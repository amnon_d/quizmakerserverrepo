const mongoose = require("mongoose");

const userName = process.env.DB_USER;
const password = process.env.DB_PASS;
const connectionString = `mongodb+srv://${userName}:${password}@cluster0.lipue.mongodb.net/quizDatabase?retryWrites=true&w=majority`;

exports.connect = async function connect() {
  try {
    await mongoose.connect(connectionString, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
    console.log("mongo connected!");
    console.log(Date());
  } catch (error) {
    console.error("mongo not connected");
  }
};
