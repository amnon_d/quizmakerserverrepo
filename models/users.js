const mongoose = require("mongoose");
const { Schema, model } = mongoose;

// schema is the rule
//model is the object. it must comply with the rule
// this helps to do the validation. won't let us enter wrong things
const userSchema = new Schema({
  userIdentifier: {
    type: Number,
    required: true,
    unique: true,
  },
  userName: {
    type: String,
    required: true,
    unique: true,
  },
  userEmail: {
    type: String,
    required: true,
    unique: true,
  },
  userPassword: {
    type: String,
    select: false,
    required: true,
  },
  isUserActive: {
    type: Boolean,
    default: true, //false stands for deleted
  },
  token: {
    type: String,
    select: false,
  },
  userLastSeen: {
    type: Date,
    default: Date.now,
  },
  userImage: {
    type: String,
    default: "https://images.app.goo.gl/D8Y6bvEdH8iPqtGv7",
  },
});

// with this model we go to the DB
// for each collection we need a model and controller
const UserSchema = model("user", userSchema);

module.exports = UserSchema; //== export default
