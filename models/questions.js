const mongoose = require("mongoose");
const { Schema, model } = mongoose;

// schema is the rule
//model is the object. it must comply with the rule
// this helps to do the validation. won't let us enter wrong things
const questionSchema = new Schema({
  questionIdentifier: {
    type: String,
    required: true,
    unique: true,
  },
  questionName: {
    type: String,
    required: true,
  },
  questionDescription: {
    type: String,
    default: "no description entered for question",
  },
  questionType: {
    type: String,
    enum: ["singleAnswer", "multipleAnswer"],
    required: true,
  },
  questionQuizIdentifier: {
    type: String,
    required: true,
  },
  isQuestionMandatory: {
    type: Boolean,
    required: true,
  },
  questionPoints: {
    type: Number,
    required: true,
  },
  // questionAnswers: {
  //   type: [{ answerIdentifier: String, correctness: Boolean }],
  //   rewired: true,
  // },
});

// with this model we go to the DB
// for each collection we need a model and controller
const QuestionSchema = model("question", questionSchema);

module.exports = QuestionSchema; //== export default
