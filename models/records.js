const mongoose = require("mongoose");
const { Schema, model } = mongoose;

// schema is the rule
//model is the object. it must comply with the rule
// this helps to do the validation. won't let us enter wrong things
const recordSchema = new Schema({
  recordIdentifier: {
    type: { userIdentifier: Number, quizIdentifier: String },
    required: true,
    unique: true,
  },
  recordMark: {
    type: Number,
  },
  recordStatus: {
    type: String,
    enum: ["initial", "inProgress", "submitted"],
    deafult: "initial",
    required: true,
  },
  recordSubmissionDate: {
    type: Date,
  },
  recordStartDate: {
    type: Date,
  },
  recordAnswers: {
    type: [{ questionIdentifier: String, answers: [String] }],
  },
});

// with this model we go to the DB
// for each collection we need a model and controller
const RecordSchema = model("record", recordSchema);

module.exports = RecordSchema; //== export default
