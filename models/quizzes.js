const mongoose = require("mongoose");
const { Schema, model } = mongoose;

// schema is the rule
//model is the object. it must comply with the rule
// this helps to do the validation. won't let us enter wrong things
const quizSchema = new Schema({
  quizIdentifier: {
    type: String,
    required: true,
    unique: true,
  },
  quizDescription: {
    type: String,
    default: "no description entered for quiz",
  },
  quizName: {
    type: String,
    required: true,
    unique: true,
  },
  quizCreationDate: {
    type: Date,
    default: Date.now,
  },
  quizCreator: {
    type: Number,
    required: true,
  },
  quizDeadLine: {
    type: Date,
    required: true,
  },
  isQuizActive: {
    type: Boolean,
    default: true, //false stands for deleted
  },
  quizFormType: {
    type: String,
    enum: ["test", "survey"],
    required: true,
  },
  quizStatus: {
    type: String,
    enum: ["created", "draft", "published"],
    deafult: "created",
    required: true,
  },
  isQuizShared: {
    type: Boolean,
    default: false,
  },
  quizLink: {
    type: String,
    required: true,
  },
  quizEntriesCounter: {
    type: Number,
    default: 0,
  },
});

// with this model we go to the DB
// for each collection we need a model and controller
const QuizSchema = model("quiz", quizSchema);

module.exports = QuizSchema; //== export default
