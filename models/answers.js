const mongoose = require("mongoose");
const { Schema, model } = mongoose;

// schema is the rule
//model is the object. it must comply with the rule
// this helps to do the validation. won't let us enter wrong things
const answerSchema = new Schema({
  answerIdentifier: {
    type: String,
    required: true,
    unique: true,
  },
  answerDescription: {
    type: String,
    default: "no description entered for answer",
  },

  isAnswerCorrect: {
    type: Boolean,
    required: true,
    select: false,
  },

  answerQuestionIdentifier: {
    //To be deleted
    type: String,
    required: true,
  },
});

// with this model we go to the DB
// for each collection we need a model and controller
const AnswerSchema = model("answer", answerSchema);

module.exports = AnswerSchema; //== export default
