const bcryptjs = require("bcryptjs");
const UserModel = require("../models/users");

const { createToken } = require("../jwt");

async function create(data) {
  try {
    data.userPassword = bcryptjs.hashSync(data.userPassword, 8);
    const newUser = await UserModel.create(data);
    return newUser;
  } catch (err) {
    console.log(err);
    throw err;
  }
}
exports.create = create;

exports.login = async function login(data) {
  try {
    console.log(data);
    // { new: true } this is because the default is that it returns the old version
    // if we want to see the value ufter the update we need to set it here
    // return await UserModel.findOneAndUpdate(_id, newData, { new: true });
    //also we did not call readOne as the teacher did because we want readOne
    // to check the token, and here we do not have a token as we are going to
    // create one.
    const user = await UserModel.findOne(
      { userEmail: data.userEmail },
      "+userPassword"
    );

    if (!user) throw "Faild to login";

    if (!bcryptjs.compareSync(data.userPassword, user.userPassword))
      throw "Wrong password";

    const token = createToken(user._id);
    user.token = token;
    user.userLastSeen = Date.now();

    const updatedUser = await UserModel.findByIdAndUpdate(user._id, user, {
      new: true,
    });

    //since token is set in the schema to select:false, we will not see it
    //we are using postman to work with this server and therefore we need
    //to see this token. this is the only reason we insert it manually in
    //the object
    updatedUser.token = token;

    return updatedUser;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

async function update(newData) {
  try {
    // { new: true } this is because the default is that it returns the old version
    // if we want to see the value ufter the update we need to set it here
    // return await UserModel.findOneAndUpdate(_id, newData, { new: true });
    return await UserModel.findByIdAndUpdate(user._id, newData, {
      new: true,
    });
  } catch (err) {
    console.log(err);
    throw err;
  }
}
exports.update = update;

async function readOne(filter) {
  try {
    return UserModel.findOne(filter);
  } catch (err) {
    console.log(err);
    throw err;
  }
}
exports.readOne = readOne;

exports.read = async function read(filter) {
  try {
    return UserModel.find(filter);
  } catch (err) {
    console.log(err);
    throw err;
  }
};

exports.del = async function del(_id) {
  try {
    return UserModel.findByIdAndDelete(_id);
  } catch (err) {
    console.log(err);
    throw err;
  }
};
