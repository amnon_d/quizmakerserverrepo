const UserModel = require("../models/users");
const Users = require("./users");
const Quizzes = require("./quizzes");
const Questions = require("./questions");
const Answers = require("./answers");
const Records = require("./records");
const { verifyToken } = require("../jwt");

async function checkToken(token) {
  const user = await UserModel.findOne({ token });
  if (user && verifyToken(user._id, token)) {
    if (user.isUserActive) return user;
  }
  // return null;
  throw "Not Connected";
}

async function createUser(new_user) {
  try {
    let res = await UserModel.find({});
    const len = res.length;
    new_user.userIdentifier = len + 1;
    res = await Users.create(new_user);
    return res;
  } catch (err) {
    console.log("exception caught in general::createUser");
    throw err;
  }
}
exports.createUser = createUser;

async function getAllUsers(token) {
  try {
    const user = await checkToken(token);

    let res = await Users.read({});
    return res;
  } catch (err) {
    console.log("exception caught in general::getAllUsers");
    throw err;
  }
}
exports.getAllUsers = getAllUsers;

async function loginUser(login_user) {
  try {
    const res = await Users.login(login_user);
    return res;
  } catch (err) {
    console.log("exception caught in general::loginUser");
    throw err;
  }
}
exports.loginUser = loginUser;

async function createQuiz(new_quiz, token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    new_quiz.quizCreator = user.userIdentifier;

    let res = await Quizzes.read({});
    const len = res.length;
    new_quiz.quizIdentifier = `QZ${len + 1}`;

    new_quiz.quizStatus = "created";
    new_quiz.quizLink = `/takequiz/QZ${len + 1}`;

    res = await Quizzes.create(new_quiz);
    return res;
  } catch (err) {
    console.log("exception caught in general::createQuiz");
    throw err;
  }
}
exports.createQuiz = createQuiz;

async function updateQuiz(quiz, token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    let res;
    // find the original quiz by its name. update works by id but the quiz
    // we received in the arguments list contains only the
    // parameters that we want to change
    res = await Quizzes.readOne({ quizIdentifier: quiz.quizIdentifier });

    if (!res.isQuizActive || res.quizStatus == "published" || res.isQuizShared)
      throw "Quiz cannot be editted as it is either deleted, published or shared";

    await Quizzes.update(res._id, quiz);
  } catch (err) {
    console.log("exception caught in general::updateQuiz");
    throw err;
  }
}
exports.updateQuiz = updateQuiz;

async function saveQuiz(quiz, questions, answers, token, publish = false) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    let res;
    let updated_quiz;
    res = await Quizzes.readOne({ quizIdentifier: quiz.quizIdentifier });

    //prepare the new status for the quiz
    if (publish) quiz.quizStatus = "published";
    else quiz.quizStatus = "draft";

    await Quizzes.update(res._id, quiz);

    // res.quizStatus is the current status of the quiz
    if (res.quizStatus == "created") {
      await createNewQuestionsAndAnswersForQuiz(questions, answers, token);
    } else if (
      // if the status is anything but "created" it means questions and answers were already created
      res.quizStatus == "draft" ||
      (res.quizStatus == "published" && !res.isQuizShared)
    ) {
      // here we will delete all questions and answers of this
      // quiz from the DB and then creatre new ones
      await deleteQuestionsAndAnswersForQuiz(quiz.quizIdentifier, token);
      await createNewQuestionsAndAnswersForQuiz(questions, answers, token);
    } else throw "quiz cannot be saved because it is already shared";
  } catch (err) {
    console.log("exception caught in general::saveQuiz");
    throw err;
  }
}
exports.saveQuiz = saveQuiz;

// solutions must be passed in the form: [{ questionIdentifier: String, answers: [String] }]
async function saveQuizSolution(quiz_id, solutions, token, submit = false) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    let record = await Records.readOne({
      recordIdentifier: {
        userIdentifier: user.userIdentifier,
        quizIdentifier: quiz_id,
      },
    });

    if (record.recordStatus == "submitted")
      throw "The test has been submitted and cannot be re-submitted";

    if (submit) record.recordStatus = "submitted";
    else record.recordStatus = "inProgress";

    record.recordSubmissionDate = Date.now();

    record.recordAnswers = solutions;

    let calc_res = await calculateMark(
      quiz_id,
      record.recordAnswers,
      token,
      "+isAnswerCorrect"
    );

    // console.log("mark returned from calculator ", calc_res.markText);
    record.recordMark = calc_res.mark;

    let updated_record = await Records.update(record._id, record);
    // console.log("updated record: ", record);
    // return { "your mark is: ": mark };
    // return `your mark: ${mark}`;
    return { record: updated_record, markText: calc_res.markText };
  } catch (err) {
    console.log("exception caught in general::saveQuizSolution");
    throw err;
  }
}
exports.saveQuizSolution = saveQuizSolution;

async function createOneQuestionAndAnswers(newQuestion, answers, token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    //save the question and the answers to the DB
    res = await Questions.create(newQuestion);

    for (let i = 0; i < answers.length; i++) {
      res = await Answers.create(answers[i]);
    }

    return res;
  } catch (err) {
    console.log("exception caught in general::createOneQuestionAndAnswers");
    throw err;
  }
}
exports.createOneQuestionAndAnswers = createOneQuestionAndAnswers;

async function updateOneQuestionAndAnswers(question, answers, token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";
    // here we will delete all questions and answers of this
    // quiz from the DB and then creatre new ones
    let quiz = await Quizzes.readOne({
      quizIdentifier: question.questionQuizIdentifier,
    });

    if (
      !quiz.isQuizActive ||
      quiz.quizStatus == "published" ||
      quiz.isQuizShared
    )
      throw "Quiz cannot be editted as it is either deleted, published or shared";

    await deleteOneQuestionAndAnswers(question.questionIdentifier, token);

    await createOneQuestionAndAnswers(question, answers, token);
  } catch (err) {
    console.log("exception caught in general::updateOneQuestionAndAnswers");
    throw err;
  }
}
exports.updateOneQuestionAndAnswers = updateOneQuestionAndAnswers;

async function copyQuiz(quiz_id, token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    let res;
    res = await Quizzes.readOne({ quizIdentifier: quiz_id });

    // duplicate selected fields to a new quiz
    let new_quiz = {
      quizDescription: res.quizDescription,
      quizName: res.quizName,
      quizCreationDate: Date.now(),
      quizFormType: res.quizFormType,
      isQuizShared: res.isQuizShared,
      quizLink: res.quizLink,
      quizEntriesCounter: 0,
    };

    //get all questions for this quiz
    const questionList = await getAllQuestionsForQuiz(quiz_id, token);

    //get all answers for this quiz
    const answersList = await getAllAnswersForQuiz(
      quiz_id,
      questionList,
      token,
      "+isAnswerCorrect"
    );

    // pack everything together and return
    return { quiz: new_quiz, questions: questionList, answers: answersList };
  } catch (err) {
    console.log("exception caught in general::copyQuiz");
    throw err;
  }
}
exports.copyQuiz = copyQuiz;

async function getQuizForEditting(quiz_id, token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    let res = await Quizzes.readOne({ quizIdentifier: quiz_id });

    if (user.userIdentifier != res.quizCreator)
      throw "you are not the owner of this quiz";

    // if (!res.isQuizActive || res.quizStatus == "published" || res.isQuizShared)
    //   throw "Quiz is either deleted, published or shared";

    return getQuiz(quiz_id, token, "+isAnswerCorrect");
  } catch (err) {
    console.log("exception caught in general::getQuizForEditting");
    throw err;
  }
}
exports.getQuizForEditting = getQuizForEditting;

async function takeAQuiz(quiz_id, token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    // get a copy of a quiz including all questions from a link that looks like this for example:
    // /quizzes/takeaquiz?identifier="QZ123"
    // let res;
    let quizToTake = await getQuiz(quiz_id, token);

    //update the quiz in the DB to indicate it is shared
    let quiz = await Quizzes.readOne({ quizIdentifier: quiz_id });
    quiz.isQuizShared = true;
    quiz.quizEntriesCounter = quiz.quizEntriesCounter + 1;
    await Quizzes.update(quiz._id, quiz);

    //first check if such record exists and if yes, return the quiz and the record.
    //and only do so if the record shows it was not submitted yet.
    // do it inside try-catch so if it fails we can continue, or return null from the CRUD
    let quizRecord = await Records.readOne({
      recordIdentifier: {
        userIdentifier: user.userIdentifier,
        quizIdentifier: quiz.quizIdentifier,
      },
    });
    if (quizRecord) {
      console.log("record was found and it is: ", quizRecord);
    } else {
      //create a record for this action
      let new_record = {
        recordIdentifier: {
          userIdentifier: user.userIdentifier,
          quizIdentifier: quiz.quizIdentifier,
        },
        recordMark: 0,
        recordStatus: "initial",
        recordStartDate: Date.now(),
      };
      console.log("record was not found, creating a new one");
      quizRecord = await Records.create(new_record);
      console.log("record was created");
    }

    // add the record to the object we return
    quizToTake.record = quizRecord;
    //quizToTake looks like this: { quiz: res, questions: questionList, answers: answersList, record: quizRecord }

    //replace the quizToTake with an object that contains the relevent fields only
    quizToTake.quiz = {
      quizIdentifier: quiz.quizIdentifier,
      quizDescription: quiz.quizDescription,
      quizName: quiz.quizName,
      quizDeadLine: quiz.quizDeadLine,
    };

    //TODO
    //we also need to remove the correctness indication of the answers
    return quizToTake;
  } catch (err) {
    console.log("exception caught in general::takeAQuiz");
    throw err;
  }
}
exports.takeAQuiz = takeAQuiz;

async function sendQuiz(new_record, token) {
  try {
    const user = await checkToken(token);
    let quizRecord = await Records.readOne({
      recordIdentifier: new_record.recordIdentifier,
    });
    if (!quizRecord) {
      quizRecord = await Records.create(new_record);
      return quizRecord;
    }
  } catch (err) {
    console.log("exception caught in general::sendQuiz");
    throw err;
  }
}
exports.sendQuiz = sendQuiz;

async function getQuizzesCreatedByUser(token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    const list = await Quizzes.read({ quizCreator: user.userIdentifier });

    return list;
  } catch (err) {
    console.log("exception caught in general::getQuizzesCreatedByUser");
    throw err;
  }
}
exports.getQuizzesCreatedByUser = getQuizzesCreatedByUser;

async function getOneQuestionAndAnswers(questionId, token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";
    const question = await Questions.readOne({
      questionIdentifier: questionId,
    });

    const answers = await getAllAnswersForQuestion(
      questionId,
      token,
      "+isAnswerCorrect"
    );

    return { question: question, answers: answers };
  } catch (err) {
    console.log("exception caught in general::getOneQuestionAndAnswers");
    throw err;
  }
}
exports.getOneQuestionAndAnswers = getOneQuestionAndAnswers;

async function getQuizzesTakenByUser(token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    let filter = "QZ";
    let regex = new RegExp(filter);
    const record_list = await Records.read({
      "recordIdentifier.userIdentifier": user.userIdentifier,
    });

    let quiz_list = [];
    for (let item of record_list) {
      const quiz = await Quizzes.readOne({
        quizIdentifier: item.recordIdentifier.quizIdentifier,
      });
      quiz_list.push(quiz);
    }

    return quiz_list;
  } catch (err) {
    console.log("exception caught in general::getQuizzesTakenByUser");
    throw err;
  }
}
exports.getQuizzesTakenByUser = getQuizzesTakenByUser;

async function getRecordsAndQuizzesTakenByUser(token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    let filter = "QZ";
    let regex = new RegExp(filter);
    const record_list = await Records.read({
      "recordIdentifier.userIdentifier": user.userIdentifier,
    });

    let elem_list = [];
    let elem;
    for (let item of record_list) {
      const quiz = await Quizzes.readOne({
        quizIdentifier: item.recordIdentifier.quizIdentifier,
      });
      elem = { record: item, quiz: quiz };
      elem_list.push(elem);
    }

    return elem_list;
  } catch (err) {
    console.log("exception caught in general::getRecordsAndQuizzesTakenByUser");
    throw err;
  }
}
exports.getRecordsAndQuizzesTakenByUser = getRecordsAndQuizzesTakenByUser;

async function updateUser(update_user, token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    const res = await Users.update(update_user);
    return res;
  } catch (err) {
    console.log("exception caught in general::updateUser");
    throw err;
  }
}
exports.updateUser = updateUser;

async function deleteQuiz(quizName, token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";

    let res;
    const quizFilter = {
      quizName: quizName,
    };

    res = await Quizzes.readOne(quizFilter);

    if (res.isQuizShared) throw "Quiz was already sent to people";

    quizFilter.isQuizActive = false;
    res = await Quizzes.update(res._id, quizFilter);
    return res;
  } catch (err) {
    console.log("exception caught in general::deleteQuiz");
    throw err;
  }
}
exports.deleteQuiz = deleteQuiz;

async function getQuizIdByName(quizName, token) {
  try {
    const user = await checkToken(token);
    // if (!user) throw "Not Connected";
    let res;
    const quizFilter = {
      quizName: quizName,
    };

    res = await Quizzes.readOne(quizFilter);
    return res.quizIdentifier;
  } catch (err) {
    console.log("exception caught in general::getQuizIdByName");
    throw err;
  }
}
exports.getQuizIdByName = getQuizIdByName;

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// The following function are used internally, therefore they are not exported
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

async function deleteQuestionsAndAnswersForQuiz(quiz_id, token) {
  try {
    let regex = new RegExp(quiz_id);
    await Questions.deleteMany({ questionIdentifier: regex });
    await Answers.deleteMany({ answerIdentifier: regex });
  } catch (err) {
    console.log(
      "exception caught in general::deleteQuestionsAndAnswersForQuiz"
    );
    throw err;
  }
}

async function deleteOneQuestionAndAnswers(questionId, token) {
  try {
    let regex = new RegExp(questionId);
    const question = await Questions.readOne({
      questionIdentifier: questionId,
    });

    await Questions.del(question._id);

    await Answers.deleteMany({ answerIdentifier: regex });
  } catch (err) {
    console.log("exception caught in general::deleteOneQuestionAndAnswers");
    throw err;
  }
}

async function createNewQuestionsAndAnswersForQuiz(questions, answers, token) {
  try {
    for (let i = 0; i < questions.length; i++) {
      res = await Questions.create(questions[i]);
    }

    for (let i = 0; i < answers.length; i++) {
      for (let j = 0; j < answers[i].length; j++) {
        res = await Answers.create(answers[i][j]);
      }
    }
  } catch (err) {
    console.log(
      "exception caught in general::createNewQuestionsAndAnswersForQuiz"
    );
    throw err;
  }
}

async function getAllQuestionsForQuiz(quiz_id, token) {
  try {
    const questionList = await Questions.read({
      questionQuizIdentifier: quiz_id,
    });

    return questionList;
  } catch (err) {
    console.log("exception caught in general::getAllQuestionsForQuiz");
    throw err;
  }
}

async function getAllAnswersForQuestion(questionId, token, projection) {
  try {
    let answersList = await Answers.read(
      { answerIdentifier: new RegExp(questionId) },
      projection
    );
    return answersList;
  } catch (err) {
    console.log("exception caught in general::getAllAnswersForQuestion");
    throw err;
  }
}

async function getAllAnswersForQuiz(quiz_id, questions, token, projection) {
  try {
    let answersList = [];
    let perQuestionList;
    // let regex;
    for (let item of questions) {
      perQuestionList = await Answers.read(
        { answerIdentifier: new RegExp(item.questionIdentifier) },
        projection
      );
      answersList.push(perQuestionList);
    }
    return answersList;
  } catch (err) {
    console.log("exception caught in general::getAllAnswersForQuiz");
    throw err;
  }
}

async function getQuiz(quiz_id, token, projection = "") {
  try {
    let res;
    res = await Quizzes.readOne({ quizIdentifier: quiz_id });

    //get all questions for this quiz
    const questionList = await getAllQuestionsForQuiz(quiz_id, token);

    //get all answers for this quiz
    const answersList = await getAllAnswersForQuiz(
      quiz_id,
      questionList,
      token,
      projection
    );

    // pack everything together and return
    return { quiz: res, questions: questionList, answers: answersList };
  } catch (err) {
    console.log("exception caught in general::getQuiz");
    throw err;
  }
}

async function calculateMark(quiz_id, recordAnswers, token, projection) {
  try {
    let regex = new RegExp(quiz_id);

    //get all answers of this quiz
    let quizQuestions = await Questions.read({ questionIdentifier: regex });
    let maxPointsPossible = 0;
    // consider only the mandatory questions for now
    for (let item of quizQuestions)
      if (item.isQuestionMandatory) maxPointsPossible += item.questionPoints;

    let quizAnswers = await Answers.read(
      { answerIdentifier: regex },
      projection
    );

    let match;
    let pointsPerCorrectAnswer;
    let accumulatedPoints = 0;
    let question;
    //[{ questionIdentifier: String, answers: [String] }]
    //iterate the list of checked answers and check their correctness
    let pointsOfThisQuestion = 0;
    for (let item of recordAnswers) {
      question = quizQuestions.find(
        (elem) => elem.questionIdentifier == item.questionIdentifier
      );

      pointsOfThisQuestion = question.questionPoints;
      pointsPerCorrectAnswer = pointsOfThisQuestion / item.answers.length;

      for (let answer of item.answers) {
        match = quizAnswers.find((elem) => elem.answerIdentifier == answer);
        if (match != undefined && match.isAnswerCorrect) {
          accumulatedPoints += pointsPerCorrectAnswer;
          // if this question was not mandatory, and the student answered correctly, the points
          // are added to the mark but the maxPointsPossible are updated too as it did not consider
          // the points for this question by default.
          // if the student did not answer it correctly, no points are added and the
          // maxPointsPossible are also left as is.
          if (!question.isQuestionMandatory) {
            maxPointsPossible += pointsPerCorrectAnswer;
          }
        }
      }
    }

    let mark = Math.round((accumulatedPoints / maxPointsPossible) * 100);
    let markText = `${mark}% (${accumulatedPoints} of max ${maxPointsPossible} )`;

    return { mark: mark, markText: markText };
  } catch (err) {
    console.log("exception caught in general::calculateMark");
    throw err;
  }
}

// For testing purposes
// async function regexTest(filter, token) {
//   try {
//     let regex = new RegExp(filter);
//     let correctAnswers = await Answers.read({ answerIdentifier: regex });
//     return correctAnswers;
//   } catch (err) {
//     throw err;
//   }
// }
// exports.regexTest = regexTest;
