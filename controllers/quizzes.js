const bcryptjs = require("bcryptjs");
const QuizModel = require("../models/quizzes");
const UserModel = require("../models/users");

async function create(data) {
  try {
    return await QuizModel.create(data);
  } catch (err) {
    console.log(err);
    throw err;
  }
}
exports.create = create;

// exports.login = async function login(data) {
//   // { new: true } this is because the default is that it returns the old version
//   // if we want to see the value ufter the update we need to set it here
//   // return await QuizModel.findOneAndUpdate(_id, newData, { new: true });
//   //also we did not call readOne as the teacher did because we want readOne
//   // to check the token, and here we do not have a token as we are going to
//   // create one.
//   const quiz = await QuizModel.findOne({ email: data.email }, "+password");

//   if (!quiz) throw "Faild to login";

//   if (!bcryptjs.compareSync(data.password, quiz.password))
//     throw "Faild to login";

//   const token = createToken(quiz._id);
//   quiz.token = token;
//   quiz.lastSeen = Date.now();

//   const updatedQuiz = await QuizModel.findByIdAndUpdate(quiz._id, quiz, {
//     new: true,
//   });

//   //since token is set in the schema to select:false, we will not see it
//   //we are using postman to work with this server and therefore we need
//   //to see this token. this is the only reason we insert it manually in
//   //the object
//   updatedQuiz.token = token;

//   return updatedQuiz;
// };

async function update(_id, newData) {
  try {
    console.log("updating quiz id: ", _id);
    console.log("newdata: ", newData);
    // { new: true } this is because the default is that it returns the old version
    // if we want to see the value ufter the update we need to set it here
    // return await QuizModel.findOneAndUpdate(_id, newData, { new: true });
    return await QuizModel.findByIdAndUpdate(_id, newData, { new: true });
  } catch (err) {
    console.log(err);
    throw err;
  }
}
exports.update = update;

async function readOne(filter) {
  try {
    return QuizModel.findOne(filter);
  } catch (err) {
    console.log(err);
    throw err;
  }
}
exports.readOne = readOne;

exports.read = async function read(filter) {
  try {
    return QuizModel.find(filter);
  } catch (err) {
    console.log(err);
    throw err;
  }
};

exports.del = async function del(_id) {
  try {
    return QuizModel.findByIdAndDelete(_id);
  } catch (err) {
    console.log(err);
    throw err;
  }
};
