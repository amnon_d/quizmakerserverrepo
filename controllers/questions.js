const bcryptjs = require("bcryptjs");
const QuestionModel = require("../models/questions");
const UserModel = require("../models/users");

async function create(data) {
  try {
    return await QuestionModel.create(data);
  } catch (err) {
    console.log(err);
    throw err;
  }
}
exports.create = create;

// exports.login = async function login(data) {
//   // { new: true } this is because the default is that it returns the old version
//   // if we want to see the value ufter the update we need to set it here
//   // return await QuestionModel.findOneAndUpdate(_id, newData, { new: true });
//   //also we did not call readOne as the teacher did because we want readOne
//   // to check the token, and here we do not have a token as we are going to
//   // create one.
//   const question = await QuestionModel.findOne(
//     { email: data.email },
//     "+password"
//   );

//   if (!question) throw "Faild to login";

//   if (!bcryptjs.compareSync(data.password, question.password))
//     throw "Faild to login";

//   const token = createToken(question._id);
//   question.token = token;
//   question.lastSeen = Date.now();

//   const updatedQuestion = await QuestionModel.findByIdAndUpdate(
//     question._id,
//     question,
//     {
//       new: true,
//     }
//   );

//   //since token is set in the schema to select:false, we will not see it
//   //we are using postman to work with this server and therefore we need
//   //to see this token. this is the only reason we insert it manually in
//   //the object
//   updatedQuestion.token = token;

//   return updatedQuestion;
// };

async function update(_id, newData) {
  try {
    // { new: true } this is because the default is that it returns the old version
    // if we want to see the value ufter the update we need to set it here
    // return await QuestionModel.findOneAndUpdate(_id, newData, { new: true });
    return await QuestionModel.findByIdAndUpdate(_id, newData, { new: true });
  } catch (err) {
    console.log(err);
    throw err;
  }
}
exports.update = update;

async function readOne(filter) {
  try {
    return QuestionModel.findOne(filter);
  } catch (err) {
    console.log(err);
    throw err;
  }
}
exports.readOne = readOne;

exports.read = async function read(filter) {
  try {
    return QuestionModel.find(filter);
  } catch (err) {
    console.log(err);
    throw err;
  }
};

exports.del = async function del(_id) {
  try {
    return QuestionModel.findByIdAndDelete(_id);
  } catch (err) {
    console.log(err);
    throw err;
  }
};

exports.deleteMany = async function deleteMany(filter) {
  try {
    return QuestionModel.deleteMany(filter);
  } catch (err) {
    console.log(err);
    throw err;
  }
};
