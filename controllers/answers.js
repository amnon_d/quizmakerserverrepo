const bcryptjs = require("bcryptjs");
const AnswerModel = require("../models/answers");
const UserModel = require("../models/users");

async function create(data) {
  try {
    return await AnswerModel.create(data);
  } catch (err) {
    console.log(err);
    throw err;
  }
}
exports.create = create;

// exports.login = async function login(data) {
//   // { new: true } this is because the default is that it returns the old version
//   // if we want to see the value ufter the update we need to set it here
//   // return await AnswerModel.findOneAndUpdate(_id, newData, { new: true });
//   //also we did not call readOne as the teacher did because we want readOne
//   // to check the token, and here we do not have a token as we are going to
//   // create one.
//   const answer = await AnswerModel.findOne({ email: data.email }, "+password");

//   if (!answer) throw "Faild to login";

//   if (!bcryptjs.compareSync(data.password, answer.password))
//     throw "Faild to login";

//   const token = createToken(answer._id);
//   answer.token = token;
//   answer.lastSeen = Date.now();

//   const updatedAnswer = await AnswerModel.findByIdAndUpdate(
//     answer._id,
//     answer,
//     {
//       new: true,
//     }
//   );

//   //since token is set in the schema to select:false, we will not see it
//   //we are using postman to work with this server and therefore we need
//   //to see this token. this is the only reason we insert it manually in
//   //the object
//   updatedAnswer.token = token;

//   return updatedAnswer;
// };

async function update(_id, newData) {
  try {
    // { new: true } this is because the default is that it returns the old version
    // if we want to see the value ufter the update we need to set it here
    // return await AnswerModel.findOneAndUpdate(_id, newData, { new: true });
    return await AnswerModel.findByIdAndUpdate(_id, newData, { new: true });
  } catch (err) {
    console.log(err);
    throw err;
  }
}
exports.update = update;

async function readOne(filter, projection) {
  try {
    return AnswerModel.findOne(filter, projection);
  } catch (err) {
    console.log(err);
    return err;
  }
}
exports.readOne = readOne;

exports.read = async function read(filter, projection) {
  try {
    return AnswerModel.find(filter, projection);
  } catch (err) {
    console.log(err);
    throw err;
  }
};

exports.del = async function del(_id) {
  try {
    return AnswerModel.findByIdAndDelete(_id);
  } catch (err) {
    console.log(err);
    throw err;
  }
};

exports.deleteMany = async function deleteMany(filter) {
  try {
    return AnswerModel.deleteMany(filter);
  } catch (err) {
    console.log(err);
    throw err;
  }
};
