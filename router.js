const User = require("./controllers/users");
const Quiz = require("./controllers/quizzes");
const Question = require("./controllers/questions");
const Answer = require("./controllers/answers");
const Record = require("./controllers/records");
const general = require("./controllers/general");

// since password is defined with select:false we will not see
// it in the returned structure, therfore if we do want to see
// it we must add "password" and it will propagate to the final
//request. we specify "+password" because we still want to see all
// other fields.
module.exports = function Router(app) {
  app.get("/name", (req, res) => {
    res.send("name:$process.env.BLABLA}");
  });
  /////////////////////////   POST APIs    ////////////////////////////////////

  //////////////////////  POST users collection //////////////////////////
  app.post("/users/:action", async function (req, res) {
    try {
      if (req.params.action == "create") {
        //the required body:
        // {userName, userEmail, userPassword}
        const user = await general.createUser(req.body);
        res.send(user);
      } else if (req.params.action == "login") {
        // required body:
        // {userEmail, userPassword}}
        // console.log(req.body);
        const user = await general.loginUser(req.body);
        res.send(user);
      } else {
        res.send(`illegal action`);
      }
    } catch (error) {
      console.log("from router: ", error);
      res.status(400).send({ error: error.message || error });
    }
  });

  //////////////////////  POST quizzes collection //////////////////////////

  app.post("/quizzes/:action", async function (req, res) {
    try {
      const token = req.headers.authorization;

      if (req.params.action == "create") {
        //required body:
        // {quizName, quizDescription, quizDeadLine, quizFormType, quizLint}
        const quiz = await general.createQuiz(req.body, token);
        res.send(quiz);
      } else if (req.params.action == "update") {
        //required body:
        // {quiz}
        const quiz = await general.updateQuiz(req.body, token);

        res.send(quiz);
      } else if (req.params.action == "save") {
        //required body:
        // {modified_quiz{quizIdentifier,quizName,quizDescription,quizDeadLine,quizFormType,quizLink}, questions[{},...], answers[[{},...]], publish{bool}}
        const quiz = await general.saveQuiz(
          req.body.modified_quiz,
          req.body.questions,
          req.body.answers,
          token,
          req.body.publish
        );
        res.send(quiz);
      } else if (req.params.action == "save_solutions") {
        //required body:
        // {quizIdentifier, solutions{[{questionIdentifier, answers[]}] ,submit{bool}}
        const mark = await general.saveQuizSolution(
          req.body.quizIdentifier,
          req.body.solutions,
          token,
          req.body.submit
        );
        res.send(mark);
      } else if (req.params.action == "send") {
        //required body:
        // {record}
        const record = await general.sendQuiz(req.body, token);
        res.send(record);
      } else {
        res.send(`illegal action`);
      }
    } catch (error) {
      console.log("from router: ", error);
      res.status(400).send({ error: error.message || error });
    }
  });

  //////////////////////  POST questions collection //////////////////////////

  app.post("/questions/:action", async function (req, res) {
    try {
      const token = req.headers.authorization;

      if (req.params.action == "create_qanda") {
        //required body:
        //{question: {question fields}, answers: [[{answer fields},...]}
        const list = await general.createOneQuestionAndAnswers(
          req.body.question,
          req.body.answers,
          token
        );
        res.send(list);
      } else if (req.params.action == "update_qanda") {
        //required body:
        //{question: {question fields}, answers: [[{answer fields},...]}
        const list = await general.updateOneQuestionAndAnswers(
          req.body.question,
          req.body.answers,
          token
        );
        res.send(list);
      }
      //AMD calling via general only
      // else if (req.params.action == "create") {
      //   const list = await Question.create(req.body);
      //   res.send(list);
      // }
      else {
        res.send(`illegal action`);
      }
    } catch (error) {
      console.log("from router: ", error);
      res.status(400).send({ error: error.message || error });
    }
  });

  //////////////////////  POST answers collection //////////////////////////
  //AMD calling via general only
  // app.post("/answers/:action", async function (req, res) {
  //   try {
  //     const token = req.headers.authorization;

  //     if (req.params.action == "create") {
  //       const list = await Answer.create(req.body);
  //       res.send(list);
  //     }
  //     else {
  //       res.send(`illegal action`);
  //     }
  //   } catch (error) {
  //     res.status(400).send({ error: error.message || error });
  //   }
  // });

  //////////////////////  POST records collection //////////////////////////
  //AMD calling via general only
  // app.post("/records/:action", async function (req, res) {
  //   try {
  //     const token = req.headers.authorization;

  //     if (req.params.action == "create") {
  //       const list = await Record.create(req.body);
  //       res.send(list);
  //     }
  //     // else if (req.params.action == "login") {
  //     //   const list = await Record.login(req.body);
  //     //   res.send(list);
  //     // }
  //     else {
  //       res.send(`illegal action`);
  //     }
  //   } catch (error) {
  //     res.status(400).send({ error: error.message || error });
  //   }
  // });

  //////////////////////////////    GET APIs   ///////////////////////////////////////

  ///////////////////////////////   GET users collection  ///////////////////////////
  app.get("/users/:action", async function (req, res) {
    try {
      const token = req.headers.authorization;

      if (req.params.action == "getall") {
        const list = await general.getAllUsers(token);
        res.send(list);
        // } else if (req.params.action == "readone") {
        //   const list = await User.readOne(req.body);
        //   res.send(list);
      } else {
        res.send(`illegal action`);
      }
    } catch (error) {
      console.log("from router: ", error);
      res.status(400).send({ error: error.message || error });
    }
  });

  ///////////////////////////////   GET quizzes collection  ///////////////////////////

  app.get("/quizzes/:action/:id", async function (req, res) {
    try {
      const token = req.headers.authorization;
      // console.log("this is the token :", token);
      if (req.params.action == "copy") {
        //required body:
        // {quizIdentifier}
        const quiz = await general.copyQuiz(req.params.id, token);
        res.send(quiz);
      } else if (req.params.action == "edit") {
        //required body:
        // {quizIdentifier}
        // console.log("passing token: \n", token);
        const quiz = await general.getQuizForEditting(req.params.id, token);
        res.send(quiz);
      } else if (req.params.action == "open") {
        //required body:
        // {quizIdentifier}
        const quiz = await general.takeAQuiz(req.params.id, token);
        res.send(quiz);
      } else if (req.params.action == "getIdByName") {
        //required param: quizName
        const quiz = await general.getQuizIdByName(req.params.id, token);
        res.send(quiz);
      }
      //AMD calling via general only
      // else if (req.params.action == "read") {
      //   const list = await Quiz.read(req.body);
      //   res.send(list);
      // } else if (req.params.action == "readone") {
      //   const list = await Quiz.readOne(req.body);
      //   res.send(list);
      // }
      else {
        res.send(`illegal action`);
      }
    } catch (error) {
      console.log("from router: ", error);
      res.status(400).send({ error: error.message || error });
    }
  });

  app.get("/quizzes/:action", async function (req, res) {
    // console.log("in req ", req);
    // console.log("in req.params ", req.params);
    // console.log("in req.params.action ", req.params.action);
    try {
      const token = req.headers.authorization;
      // console.log("this is the token :", token);
      if (req.params.action == "getCreatedByUser") {
        //required body:
        // nothing, just token
        // console.log(token);
        const list = await general.getQuizzesCreatedByUser(token);
        res.send(list);
      }
    } catch (error) {
      console.log("from router: ", error);
      res.status(400).send({ error: error.message || error });
    }
  });
  ///////////////////////////////   GET questions collection  ///////////////////////////

  app.get("/questions/:action/:questionid", async function (req, res) {
    try {
      const token = req.headers.authorization;

      if (req.params.action == "edit") {
        const list = await general.getOneQuestionAndAnswers(
          req.params.questionid,
          token
        );
        res.send(list);
      } else {
        res.send(`illegal action`);
      }
    } catch (error) {
      console.log("from router: ", error);
      res.status(400).send({ error: error.message || error });
    }
  });

  //AMD calling via general only
  // app.get("/questions/:action", async function (req, res) {
  //   try {
  //     const token = req.headers.authorization;

  //     if (req.params.action == "read") {
  //       const list = await Question.read(req.body);
  //       res.send(list);
  //     } else if (req.params.action == "readone") {
  //       const list = await Question.readOne(req.body);
  //       res.send(list);
  //     } else {
  //       res.send(`illegal action`);
  //     }
  //   } catch (error) {
  //     res.status(400).send({
  //       error: error.message || error,
  //     });
  //   }
  // });

  ///////////////////////////////   GET abswers collection  ///////////////////////////
  //AMD calling via general only

  // app.get("/answers/:action", async function (req, res) {
  //   try {
  //     const token = req.headers.authorization;

  //     if (req.params.action == "read") {
  //       const list = await Answer.read(req.body);
  //       res.send(list);
  //     } else if (req.params.action == "readone") {
  //       const list = await Answer.readOne(req.body);
  //       res.send(list);
  //     } else {
  //       res.send(`illegal action`);
  //     }
  //   } catch (error) {
  //     res.status(400).send({
  //       error: error.message || error,
  //     });
  //   }
  // });

  ///////////////////////////////   GET records collection  ///////////////////////////

  app.get("/records/:action", async function (req, res) {
    try {
      const token = req.headers.authorization;

      if (req.params.action == "getTakenByUser") {
        //required body:
        // nothing, just token
        // console.log(token);
        const list = await general.getQuizzesTakenByUser(token);
        res.send(list);
      } else if (req.params.action == "getRcAndQzTakenByUser") {
        //required body:
        // nothing, just token
        // console.log(token);
        const list = await general.getRecordsAndQuizzesTakenByUser(token);
        res.send(list);
      }
      //AMD calling via general only
      // else if (req.params.action == "read") {
      //   const list = await Record.read(req.body);
      //   res.send(list);
      // } else if (req.params.action == "readone") {
      //   const list = await Record.readOne(req.body);
      //   res.send(list);
      // }
      else {
        res.send(`illegal action`);
      }
    } catch (error) {
      console.log("from router: ", error);
      res.status(400).send({ error: error.message || error });
    }
  });

  ////////////////////////////////////    PUT APIs   ////////////////////////////////////////////

  ////////////////////////////////    PUT users collection   ////////////////////////////////
  app.put("/users/:action", async function (req, res) {
    try {
      const token = req.headers.authorization;

      if (req.params.action == "update") {
        // required body:
        // {userName, userImage, any other user's field }
        const user = await general.updateUser(req.body, token);
        res.send(user);
      } else {
        res.send(`illegal action`);
      }
    } catch (error) {
      console.log("from router: ", error);
      res.status(400).send({ error: error.message || error });
    }
  });

  ////////////////////////////////    PUT quizzes collection   ////////////////////////////////
  //AMD calling via general only
  // app.put("/quizzes/:action", async function (req, res) {
  //   try {
  //     const token = req.headers.authorization;

  //     if (req.params.action == "update") {
  //       const id = req.query.id;
  //       const list = await Quiz.update(id, req.body);
  //       res.send(list);
  //     } else {
  //       res.send(`illegal action`);
  //     }
  //   } catch (error) {
  //     res.status(400).send({
  //       error: error.message || error,
  //     });
  //   }
  // });

  ////////////////////////////////    PUT questions collection   ////////////////////////////////
  //AMD calling via general only
  // app.put("/questions/:action", async function (req, res) {
  //   try {
  //     const token = req.headers.authorization;

  //     if (req.params.action == "update") {
  //       const id = req.query.id;
  //       const list = await Question.update(id, req.body);
  //       res.send(list);
  //     } else {
  //       res.send(`illegal action`);
  //     }
  //   } catch (error) {
  //     res.status(400).send({
  //       error: error.message || error,
  //     });
  //   }
  // });

  ////////////////////////////////    PUT answers collection   ////////////////////////////////
  //AMD calling via general only
  // app.put("/answers/:action", async function (req, res) {
  //   try {
  //     const token = req.headers.authorization;

  //     if (req.params.action == "update") {
  //       const id = req.query.id;
  //       const list = await Answer.update(id, req.body);
  //       res.send(list);
  //     } else {
  //       res.send(`illegal action`);
  //     }
  //   } catch (error) {
  //     res.status(400).send({
  //       error: error.message || error,
  //     });
  //   }
  // });

  ////////////////////////////////    PUT records collection   ////////////////////////////////
  //AMD calling via general only
  // app.put("/records/:action", async function (req, res) {
  //   try {
  //     const token = req.headers.authorization;

  //     if (req.params.action == "update") {
  //       const id = req.query.id;
  //       const list = await Record.update(id, req.body);
  //       res.send(list);
  //     } else {
  //       res.send(`illegal action`);
  //     }
  //   } catch (error) {
  //     res.status(400).send({
  //       error: error.message || error,
  //     });
  //   }
  // });

  /////////////////////////////DELETE APIs ////////////////////////
  app.delete("/quizzes/:action", async function (req, res) {
    try {
      const token = req.headers.authorization;

      if (req.params.action == "delete") {
        // required body:
        // {quizName}
        const quiz = await general.deleteQuiz(req.body, token);
        res.send(quiz);
      } else {
        res.send(`illegal action`);
      }
    } catch (error) {
      console.log("from router: ", error);
      res.status(400).send({ error: error.message || error });
    }
  });
};
