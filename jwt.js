const jwt = require("jsonwebtoken");

// const secret = "6534789244357"; //some key to open the token or process.env.SECRET
const secret = process.env.JWT_SECRET;

exports.createToken = (id) => {
  //the id is what we want to hide. we can also add the ip for example
  const token = jwt.sign({ id }, secret, /*options*/ { expiresIn: "10d" });
  return token;
};

exports.verifyToken = (id, token) => {
  const tokenData = jwt.verify(token, secret) || {};
  if (id != tokenData.id || Date.now() > tokenData.exp * 1000)
    // tokenData.exp /*num of secs*/)
    throw { error: "token unauthorised" };

  return true;
};
