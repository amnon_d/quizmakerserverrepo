require("dotenv").config();
const express = require("express");
const { connect } = require("./db");
const app = express();
const router = require("./router");

app.use(express.json());

const cors = require("cors");
app.use(cors());

//this instruct the server to run the default index.html in public from localhost:3000
app.use(express.static("public"));

console.log("conecting...");
connect().then(() => {
  router(app);
});

// app.listen(4000, () => console.log("server is up!"));
app.listen(process.env.PORT || 4000, () => console.log("server is up!"));
